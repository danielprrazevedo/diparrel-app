FROM node:10.15

ADD ./ /var/app

WORKDIR /var/app

RUN npm i && \
    npx nest build

CMD node dist/main.js